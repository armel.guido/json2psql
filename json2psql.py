#!/usr/bin/python

import simplejson
from collections import OrderedDict
import psycopg2

class json():
    def __init__(self, json_archive):
        with open(json_archive, "r") as fil:
            self.json_py = simplejson.load(fil)
            fil.close()
        self.dsn = 'host=your_host dbname=your_db_name user=your_user'
    #---------------------------------------------------
    def json2dict(self, name, Json):
        list_with_dict = False
        num = 1
        list_of_dicts = []
        if Json == 0:
            Json = self.json_py
        else:
            Json = [Json]
        for json in Json:
            dicts = {}
            if type(json) == dict:
                for key in json.keys():
                    typ = type(json[key])
                    if typ == list:
                        try:
                            b = self.json2dict(key, json[key])
                            for d in b[0].keys():
                                dicts.update({d:b[0][d]})
                                json[key].pop(d)
                        except:
                            pass # here are a little lost information
                    if type(json[key]) == dict:
                        dicts.update({key:json[key]})
            if type(json) == list:
                for element in json:
                    typ = type(element)
                    if typ == dict:
                        dicts.update({name:element})
                    if typ == list:
                        try:
                            b = self.browse(name, element)
                            for d in b[0]:
                                dicts.update(d)
                                element.pop(d)
                        except:
                            pass # here too
            dicts.update({name:json})
            list_of_dicts.append(dicts)
        return list_of_dicts
    #--------------------------------------------------
    def Create_DB(self, TABLE, first_columns, commands=[]):
        self.table = TABLE
        h = self.json2dict(self.table, 0)
        tab = {}
        commands = []
        for group in h:
            tab.update({x:group[x] for x in OrderedDict.fromkeys(group.keys()) if x not in tab.keys()})
            self.tabs = {x:x for x in tab.keys()}
        for n in range(len(h)):
            for k in tab.keys():
                if type(tab[k]) == list:
                    commands.append("CREATE TABLE "+k+" ("+k+" text);")
                    del tab[k]
                elif k in h[n].keys():
                    for camp in h[n][k]:
                        tab[k].setdefault(camp, h[n][k][camp])
        for x in tab.keys():
            command = ["CREATE TABLE ", x, [" (", first_columns], ");"]
            for column in tab[x].keys():
                typ = type(tab[x][column])
                if typ == str or typ == list:
                    command[2].append(column+" text, ")
                if typ == int:
                    command[2].append(column+" real, ")
                if typ == bool:
                    command[2].append(column+" bool, ")
            command[2][-1] = command[2][-1][:-2]
            command[2] = "".join(command[2])
            commands.append("".join(command))
            del tab[x]
        self.h = h
        return commands
    #----------------------------------------------------------------------
    def Load_DB(self, first_col, commands=[]):
        new_columns = {x:[] for x in self.tabs.keys()}
        column_names = {}
        commands = []
        h = self.h
        for camp in self.tabs.keys():
            with psycopg2.connect(self.dsn) as connection:
                with connection.cursor() as cur:
                    cur.execute("select column_name from information_schema.columns where table_name='"+self.tabs[camp]+"'")
                    column_names.upgrade({camp:[row[0] for row in cur]})
        for n in range(len(h)):
            for k in h[n].keys():
                if type(h[n][k]) == list:
                    commands.append("INSERT INTO "+k+" ("+first_col[0]+k+") VALUES ("+first_col[1]+"$$"+str(h[n][k])+"$$);")
                    del h[n][k]
        for x in range(len(h)):
            for camp in h[x].keys():
                command = ["INSERT INTO ", camp, [" (", first_col[0]], [") VALUES (", first_col[1]], ");"]
                for column in h[x][camp].keys():
                    typ = type(h[x][camp][column])
                    if typ == str or typ == list:
                        command[2].append(column+", ")
                        command[3].append("$$"+str(h[x][camp][column])+"$$, ")
                        if column not in column_names[camp]:
                            if column+" text, " not in new_columns[camp]:
                                new_columns[camp].append(column+" text, ")
                    if typ == int:
                        command[2].append(column+", ")
                        command[3].append(str(h[x][camp][column])+", ")
                        if column not in column_names[camp]:
                            if column+" real, " not in new_columns[camp]:
                                new_columns[camp].append(column+" real, ")
                    if typ == bool:
                        command[2].append(column+", ")
                        command[3].append(str(h[x][camp][column])+", ".lower())
                        if column not in column_names[camp]:
                            if column+" bool, " not in new_columns[camp]:
                                new_columns[camp].append(column+" bool, ")
                command[2][-1] = command[2][-1][:-2]
                command[2] = "".join(command[2])
                command[3][-1] = command[3][-1][:-2]
                command[3] = "".join(command[3])
                commands.append("".join(command))
                del h[x][camp]
        for camp in new_columns:
            commands.append("ALTER TABLE "+self.tabs[camp]+" ADD COLUMN "+"ADD COLUMN ".join(new_columns[camp])[:-2]+";")
        commands.sort(reverse=False)
        return commands