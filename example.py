#!/usr/bin/python

from json_manager import *
import os

j = json("file.json")
l = j.Create_DB("table_name", "my_id bigint default nextval('my_id'), ")
for x in range(len(l)):
    l[x] = l[x].split(" ")
    if l[x][2] == "user":
        l[x][2] = "usuario"
    l[x] = " ".join(l[x])
with open("create_db.sql", "w") as fil:
    fil.write("\n".join(l))
    fil.close()
j.tabs['user'] = 'usuario'

os.system("psql -w -h yourhost -U your_user your_db -c 'DROP TABLE something;'")
os.system("psql -w -h yourhost -U your_user your_db -c 'DROP SEQUENCE my_id;'")
os.system("psql -w -h yourhost -U your_user your_db -c 'CREATE SEQUENCE my_id'")
os.system("psql -w -h yourhost -U your_user your_db -f 'create_db.sql'")

g = j.Load_DB(("", ""))
for x in range(len(g)):
    g[x] = g[x].split(" ")
    if g[x][2] == "user":
        g[x][2] = "usuario"
    g[x] = " ".join(g[x])
with open("load_db.sql", "w") as fil:
    fil.write("\n".join(g))
    fil.close()

os.system("psql -w -h yourhost -U your_user your_db -f 'load_db.sql'")